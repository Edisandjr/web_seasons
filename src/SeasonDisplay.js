import './SeasonDisplay.css';
import React from "react";

const SeasonDisplay = (props) => {
    const season = getSeason(props.latt, new Date().getMonth());
    const { seasonMessage, seasonIcon } = seasonConfig[season];

    return (
        <div className={`season-display ${season}`}>
            <i className={`icon-left massive ${seasonIcon} icon`} />
            <h1>{seasonMessage}</h1>
            <i className={`icon-right massive ${seasonIcon} icon`} />
        </div>
    );
};

const seasonConfig = {
    summer: {
        seasonMessage: "Time for the beach!",
        seasonIcon: "sun",
    },
    winter: {
        seasonMessage: "It's cold outside!",
        seasonIcon: "snowflake",
    },
};

const getSeason = (latt, month) => {
    if (month > 2 && month < 9) {
        return latt > 0 ? "summer" : "winter";
    } else {
        return latt > 0 ? "winter" : "summer";
    }
};


export default SeasonDisplay;
