import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import Loader from "./Loader.js";

class App extends React.Component {
  state = { latt: null, errorMessage: "" };
  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      (position) => this.setState({ latt: position.coords.latitude }),
      (err) => this.setState({ errMessage: err.message })
    );
  }
  renderContent() {
    if (this.state.errMessage && !this.state.latt) {
      return <div>Error:{this.state.errMessage}</div>;
    }
    if (!this.state.errMessage && this.state.latt) {
      return <SeasonDisplay latt={this.state.latt} />;
    }
    return <Loader message="Please accept location request" />;
  }
  render() {
    return <div>{this.renderContent()}</div>;
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
